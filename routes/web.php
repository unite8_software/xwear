<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('customerApi/{path}','CustomerApi@'.$path);

// Route::get('customerApi/{path}',function($path = null) {
//     return redirect()->action("customerApi/CustomerApi@$path");
// });

Route::get('customerApi/{path}',function() {
    return redirect()->action('CustomerApi@index');
 });

Route::get('customerApi','CustomerApi@index');