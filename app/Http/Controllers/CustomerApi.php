<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Customer;

class CustomerApi extends Controller
{
    //
    function index()
    {
        return view('customerApi', ["SITE_URL" => URL::current()]);
    }

    function abc()
    {
        echo "Abc";
    }

    function sendOtp(Request $req)
    {
        echo "123";
        exit;

        $get_user =  Customer::where("phone_number", $req->input('mobile'))->get();
        print_r($get_user);
        exit;

        if (!$get_user->id) {
            $data['code'] = FAILURE;
            $data['message'] = 'No User found';
            header('Content-type: application/json');
            echo json_encode($data);
            exit();
        }
        $authKey = "75119AO8rdIhvuv54804b27";
        $senderId = "ZepoIn";
        $route = "4";
        $otp = rand(1000, 9999);
        $message = 'your otp is ' . $otp;
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobile,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        $url = "https://control.msg91.com/sendhttp.php";

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));

        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);
        if (curl_errno($ch)) {
            $data['code'] = FAILURE;
            $data['message'] = 'Something went wrong';
            header('Content-type: application/json');
            echo json_encode($data);
            exit();
        }
        curl_close($ch);

        $this->Common_model->update(TBL_USERS, array('otp' => $otp), array('id' => $get_user->id));

        $data['code'] = SUCCESS;
        $data['message'] = 'OTP sent successfully';
        header('Content-type: application/json');
        echo json_encode($data);
    }
}
